## Objective

- Today I mainly learned Spring Data JPA (Java Persistence API) and used it to manipulate the database instead of the in-memory data implemented yesterday.

## Reflective

- Today is a relatively easy day, even if I am exposed to new technologies, but because I have been exposed to similar Object-Relational Mapping frameworks before, it is relatively quick to get started.

## Interpretive

- JPA is more convenient to use than other ORM frameworks, such as:
  - By using the `@Entity` annotation, developers can map objects to database tables, greatly reducing the effort of writing SQL statements manually.
  - Use `@OneToOne`, `@OneToMany`, and `@ManyToMany` annotations to label relationship mappings. These annotations are often used in conjunction with `@JoinColumn` annotations to specify the associated foreign key field.
  - Automatic implementation (CRUD) operation, only need to define the interface, inherit the corresponding Repository interface, you can use built-in methods for data persistence and retrieval.
- While Spring Data JPA offers many advantages, it also has some limitations. For example, it may be difficult to process complex dynamic queries, or scenarios with very high performance requirements may require more flexible approaches such as native SQL or MyBatis.

## Decisional

- Practice more Spring Data JPA focusing on solutions and the use of interfaces.